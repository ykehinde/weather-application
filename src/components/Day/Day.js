import React, { Component } from 'react';
import './Day.scss';
import WeatherIcon from '../svgIcons'

class Day extends Component {
  constructor(props) {
    super(props);
  }

  render() {
  	return (
  		<div className="day">
        <h3>{this.props.day}</h3>
        <h4>{this.props.date}</h4>
        <WeatherIcon icon={this.props.icon} className="weatherIcon" style={{width: '100px'}} />
        <p>{this.props.weather}</p>
        <p>{this.props.temp}&deg;C</p>
        <p className="low-temp">{this.props.low}&deg;C</p>
      </div>
  	);
  }
}
export default Day;