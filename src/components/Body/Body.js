import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as dayjs from 'dayjs';

import {
  getWeather
} from '../../app/actions';
import "./Body.scss";
import Day from "../Day/Day";
import {
  sampleData,
  months
} from "../constants.js";

class Body extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    }
  }

  _normaliseDate(date){
    let newDate = date.split('-');
    let dateMonth = months[newDate[0]-1]
    let dateday = newDate[1];
    return `${dateMonth} ${dateday}`
  }
  
    componentDidMount() {
      getWeather();
    }

  render() {
    // const {
    //   // dataReady,
    //   weatherData,
    //   callerError,
    //   weatherSpinnerOpen
    // } = this.props;

    const days = [];

    for (let i = 0; i < sampleData[0].list.length; i++) {
      const midday = sampleData[0].list[i];
      console.log(midday.weather[0]);
      if (midday.dt_txt.includes("12:00:00")) {
        days.push(midday);
      }
    }

  	return (
  		<div className="body">
        <h2>5 Day Forecast</h2>
      {
        days.map((day, i) => (
          <Day
            key= "i"
            date={this._normaliseDate(day.dt_txt.slice(5, 10))}
            icon={day.weather[0].main.toLowerCase()}
            temp={day.main.temp}
            low={day.main.temp_min}
            weather={day.weather[0].description}
             />
        ))
      }
      </div>
  	);
  }
}

const mapStateToProps = (state) => {
  return {
    weatherData: state.data.forecast.data,
    callerError: state.data.callerError,
    weatherSpinnerOpen: state.show.weatherSpinner
  }
}

export default connect(mapStateToProps)(Body);