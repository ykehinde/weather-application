export const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

export const sampleData = [{
  "cod": "200",
  "message": 0,
  "cnt": 40,
  "list": [{
    "dt": 1605376800,
    "main": {
      "temp": 14.22,
      "feels_like": 8.2,
      "temp_min": 14.22,
      "temp_max": 14.56,
      "pressure": 1004,
      "sea_level": 1004,
      "grnd_level": 1000,
      "humidity": 78,
      "temp_kf": -0.34
    },
    "weather": [{
      "id": 501,
      "main": "Rain",
      "description": "moderate rain",
      "icon": "10n"
    }],
    "clouds": {
      "all": 87
    },
    "wind": {
      "speed": 8.84,
      "deg": 196
    },
    "visibility": 10000,
    "pop": 0.98,
    "rain": {
      "3h": 3.26
    },
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-14 18:00:00"
  }, {
    "dt": 1605387600,
    "main": {
      "temp": 14.05,
      "feels_like": 9.38,
      "temp_min": 14.05,
      "temp_max": 14.1,
      "pressure": 1002,
      "sea_level": 1002,
      "grnd_level": 1000,
      "humidity": 82,
      "temp_kf": -0.05
    },
    "weather": [{
      "id": 501,
      "main": "Rain",
      "description": "moderate rain",
      "icon": "10n"
    }],
    "clouds": {
      "all": 95
    },
    "wind": {
      "speed": 7.14,
      "deg": 215
    },
    "visibility": 10000,
    "pop": 1,
    "rain": {
      "3h": 3.3
    },
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-14 21:00:00"
  }, {
    "dt": 1605398400,
    "main": {
      "temp": 13.68,
      "feels_like": 8.82,
      "temp_min": 13.67,
      "temp_max": 13.68,
      "pressure": 1002,
      "sea_level": 1002,
      "grnd_level": 999,
      "humidity": 70,
      "temp_kf": 0.01
    },
    "weather": [{
      "id": 500,
      "main": "Rain",
      "description": "light rain",
      "icon": "10n"
    }],
    "clouds": {
      "all": 84
    },
    "wind": {
      "speed": 6.38,
      "deg": 219
    },
    "visibility": 10000,
    "pop": 1,
    "rain": {
      "3h": 0.18
    },
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-15 00:00:00"
  }, {
    "dt": 1605409200,
    "main": {
      "temp": 12.8,
      "feels_like": 8.91,
      "temp_min": 12.79,
      "temp_max": 12.8,
      "pressure": 999,
      "sea_level": 999,
      "grnd_level": 996,
      "humidity": 84,
      "temp_kf": 0.01
    },
    "weather": [{
      "id": 804,
      "main": "Clouds",
      "description": "overcast clouds",
      "icon": "04n"
    }],
    "clouds": {
      "all": 97
    },
    "wind": {
      "speed": 5.68,
      "deg": 194
    },
    "visibility": 10000,
    "pop": 0.21,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-15 03:00:00"
  }, {
    "dt": 1605420000,
    "main": {
      "temp": 14.51,
      "feels_like": 8,
      "temp_min": 14.51,
      "temp_max": 14.51,
      "pressure": 995,
      "sea_level": 995,
      "grnd_level": 993,
      "humidity": 84,
      "temp_kf": 0
    },
    "weather": [{
      "id": 501,
      "main": "Rain",
      "description": "moderate rain",
      "icon": "10n"
    }],
    "clouds": {
      "all": 98
    },
    "wind": {
      "speed": 10.11,
      "deg": 196
    },
    "visibility": 10000,
    "pop": 1,
    "rain": {
      "3h": 7.29
    },
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-15 06:00:00"
  }, {
    "dt": 1605430800,
    "main": {
      "temp": 13.66,
      "feels_like": 7.51,
      "temp_min": 13.66,
      "temp_max": 13.66,
      "pressure": 994,
      "sea_level": 994,
      "grnd_level": 991,
      "humidity": 71,
      "temp_kf": 0
    },
    "weather": [{
      "id": 500,
      "main": "Rain",
      "description": "light rain",
      "icon": "10d"
    }],
    "clouds": {
      "all": 100
    },
    "wind": {
      "speed": 8.29,
      "deg": 214
    },
    "visibility": 10000,
    "pop": 0.96,
    "rain": {
      "3h": 1.64
    },
    "sys": {
      "pod": "d"
    },
    "dt_txt": "2020-11-15 09:00:00"
  }, {
    "dt": 1605441600,
    "main": {
      "temp": 11.47,
      "feels_like": 5.49,
      "temp_min": 11.47,
      "temp_max": 11.47,
      "pressure": 995,
      "sea_level": 995,
      "grnd_level": 992,
      "humidity": 58,
      "temp_kf": 0
    },
    "weather": [{
      "id": 500,
      "main": "Rain",
      "description": "light rain",
      "icon": "10d"
    }],
    "clouds": {
      "all": 100
    },
    "wind": {
      "speed": 6.52,
      "deg": 244
    },
    "visibility": 10000,
    "pop": 0.94,
    "rain": {
      "3h": 0.59
    },
    "sys": {
      "pod": "d"
    },
    "dt_txt": "2020-11-15 12:00:00"
  }, {
    "dt": 1605452400,
    "main": {
      "temp": 11.41,
      "feels_like": 4.7,
      "temp_min": 11.41,
      "temp_max": 11.41,
      "pressure": 994,
      "sea_level": 994,
      "grnd_level": 992,
      "humidity": 55,
      "temp_kf": 0
    },
    "weather": [{
      "id": 802,
      "main": "Clouds",
      "description": "scattered clouds",
      "icon": "03d"
    }],
    "clouds": {
      "all": 44
    },
    "wind": {
      "speed": 7.36,
      "deg": 227
    },
    "visibility": 10000,
    "pop": 0.33,
    "sys": {
      "pod": "d"
    },
    "dt_txt": "2020-11-15 15:00:00"
  }, {
    "dt": 1605463200,
    "main": {
      "temp": 8.62,
      "feels_like": 6.09,
      "temp_min": 8.62,
      "temp_max": 8.62,
      "pressure": 994,
      "sea_level": 994,
      "grnd_level": 991,
      "humidity": 80,
      "temp_kf": 0
    },
    "weather": [{
      "id": 501,
      "main": "Rain",
      "description": "moderate rain",
      "icon": "10n"
    }],
    "clouds": {
      "all": 72
    },
    "wind": {
      "speed": 2.11,
      "deg": 320
    },
    "visibility": 10000,
    "pop": 0.79,
    "rain": {
      "3h": 4.62
    },
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-15 18:00:00"
  }, {
    "dt": 1605474000,
    "main": {
      "temp": 9.06,
      "feels_like": 2.7,
      "temp_min": 9.06,
      "temp_max": 9.06,
      "pressure": 998,
      "sea_level": 998,
      "grnd_level": 995,
      "humidity": 69,
      "temp_kf": 0
    },
    "weather": [{
      "id": 500,
      "main": "Rain",
      "description": "light rain",
      "icon": "10n"
    }],
    "clouds": {
      "all": 67
    },
    "wind": {
      "speed": 7.11,
      "deg": 247
    },
    "visibility": 10000,
    "pop": 0.79,
    "rain": {
      "3h": 0.63
    },
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-15 21:00:00"
  }, {
    "dt": 1605484800,
    "main": {
      "temp": 10.4,
      "feels_like": 4.88,
      "temp_min": 10.4,
      "temp_max": 10.4,
      "pressure": 1000,
      "sea_level": 1000,
      "grnd_level": 998,
      "humidity": 81,
      "temp_kf": 0
    },
    "weather": [{
      "id": 803,
      "main": "Clouds",
      "description": "broken clouds",
      "icon": "04n"
    }],
    "clouds": {
      "all": 70
    },
    "wind": {
      "speed": 6.98,
      "deg": 238
    },
    "visibility": 10000,
    "pop": 0.61,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-16 00:00:00"
  }, {
    "dt": 1605495600,
    "main": {
      "temp": 11.25,
      "feels_like": 4.32,
      "temp_min": 11.25,
      "temp_max": 11.25,
      "pressure": 1004,
      "sea_level": 1004,
      "grnd_level": 1001,
      "humidity": 67,
      "temp_kf": 0
    },
    "weather": [{
      "id": 500,
      "main": "Rain",
      "description": "light rain",
      "icon": "10n"
    }],
    "clouds": {
      "all": 97
    },
    "wind": {
      "speed": 8.39,
      "deg": 265
    },
    "visibility": 10000,
    "pop": 0.53,
    "rain": {
      "3h": 0.36
    },
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-16 03:00:00"
  }, {
    "dt": 1605506400,
    "main": {
      "temp": 9.11,
      "feels_like": 2.42,
      "temp_min": 9.11,
      "temp_max": 9.11,
      "pressure": 1008,
      "sea_level": 1008,
      "grnd_level": 1006,
      "humidity": 68,
      "temp_kf": 0
    },
    "weather": [{
      "id": 803,
      "main": "Clouds",
      "description": "broken clouds",
      "icon": "04n"
    }],
    "clouds": {
      "all": 74
    },
    "wind": {
      "speed": 7.55,
      "deg": 271
    },
    "visibility": 10000,
    "pop": 0.33,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-16 06:00:00"
  }, {
    "dt": 1605517200,
    "main": {
      "temp": 9.4,
      "feels_like": 3.17,
      "temp_min": 9.4,
      "temp_max": 9.4,
      "pressure": 1012,
      "sea_level": 1012,
      "grnd_level": 1009,
      "humidity": 63,
      "temp_kf": 0
    },
    "weather": [{
      "id": 800,
      "main": "Clear",
      "description": "clear sky",
      "icon": "01d"
    }],
    "clouds": {
      "all": 7
    },
    "wind": {
      "speed": 6.68,
      "deg": 269
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "d"
    },
    "dt_txt": "2020-11-16 09:00:00"
  }, {
    "dt": 1605528000,
    "main": {
      "temp": 10.81,
      "feels_like": 4.92,
      "temp_min": 10.81,
      "temp_max": 10.81,
      "pressure": 1015,
      "sea_level": 1015,
      "grnd_level": 1012,
      "humidity": 56,
      "temp_kf": 0
    },
    "weather": [{
      "id": 802,
      "main": "Clouds",
      "description": "scattered clouds",
      "icon": "03d"
    }],
    "clouds": {
      "all": 46
    },
    "wind": {
      "speed": 6.12,
      "deg": 264
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "d"
    },
    "dt_txt": "2020-11-16 12:00:00"
  }, {
    "dt": 1605538800,
    "main": {
      "temp": 11.33,
      "feels_like": 6.23,
      "temp_min": 11.33,
      "temp_max": 11.33,
      "pressure": 1016,
      "sea_level": 1016,
      "grnd_level": 1013,
      "humidity": 63,
      "temp_kf": 0
    },
    "weather": [{
      "id": 804,
      "main": "Clouds",
      "description": "overcast clouds",
      "icon": "04d"
    }],
    "clouds": {
      "all": 100
    },
    "wind": {
      "speed": 5.55,
      "deg": 250
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "d"
    },
    "dt_txt": "2020-11-16 15:00:00"
  }, {
    "dt": 1605549600,
    "main": {
      "temp": 10.24,
      "feels_like": 6.02,
      "temp_min": 10.24,
      "temp_max": 10.24,
      "pressure": 1017,
      "sea_level": 1017,
      "grnd_level": 1014,
      "humidity": 70,
      "temp_kf": 0
    },
    "weather": [{
      "id": 804,
      "main": "Clouds",
      "description": "overcast clouds",
      "icon": "04n"
    }],
    "clouds": {
      "all": 100
    },
    "wind": {
      "speed": 4.42,
      "deg": 235
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-16 18:00:00"
  }, {
    "dt": 1605560400,
    "main": {
      "temp": 10.66,
      "feels_like": 6.23,
      "temp_min": 10.66,
      "temp_max": 10.66,
      "pressure": 1018,
      "sea_level": 1018,
      "grnd_level": 1015,
      "humidity": 77,
      "temp_kf": 0
    },
    "weather": [{
      "id": 804,
      "main": "Clouds",
      "description": "overcast clouds",
      "icon": "04n"
    }],
    "clouds": {
      "all": 100
    },
    "wind": {
      "speed": 5.26,
      "deg": 224
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-16 21:00:00"
  }, {
    "dt": 1605571200,
    "main": {
      "temp": 11.06,
      "feels_like": 6.82,
      "temp_min": 11.06,
      "temp_max": 11.06,
      "pressure": 1018,
      "sea_level": 1018,
      "grnd_level": 1016,
      "humidity": 81,
      "temp_kf": 0
    },
    "weather": [{
      "id": 804,
      "main": "Clouds",
      "description": "overcast clouds",
      "icon": "04n"
    }],
    "clouds": {
      "all": 100
    },
    "wind": {
      "speed": 5.36,
      "deg": 228
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-17 00:00:00"
  }, {
    "dt": 1605582000,
    "main": {
      "temp": 11.72,
      "feels_like": 7.47,
      "temp_min": 11.72,
      "temp_max": 11.72,
      "pressure": 1019,
      "sea_level": 1019,
      "grnd_level": 1016,
      "humidity": 78,
      "temp_kf": 0
    },
    "weather": [{
      "id": 804,
      "main": "Clouds",
      "description": "overcast clouds",
      "icon": "04n"
    }],
    "clouds": {
      "all": 100
    },
    "wind": {
      "speed": 5.41,
      "deg": 227
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-17 03:00:00"
  }, {
    "dt": 1605592800,
    "main": {
      "temp": 11.44,
      "feels_like": 7.63,
      "temp_min": 11.44,
      "temp_max": 11.44,
      "pressure": 1019,
      "sea_level": 1019,
      "grnd_level": 1016,
      "humidity": 77,
      "temp_kf": 0
    },
    "weather": [{
      "id": 804,
      "main": "Clouds",
      "description": "overcast clouds",
      "icon": "04n"
    }],
    "clouds": {
      "all": 98
    },
    "wind": {
      "speed": 4.63,
      "deg": 227
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-17 06:00:00"
  }, {
    "dt": 1605603600,
    "main": {
      "temp": 11.91,
      "feels_like": 7.72,
      "temp_min": 11.91,
      "temp_max": 11.91,
      "pressure": 1019,
      "sea_level": 1019,
      "grnd_level": 1016,
      "humidity": 75,
      "temp_kf": 0
    },
    "weather": [{
      "id": 804,
      "main": "Clouds",
      "description": "overcast clouds",
      "icon": "04d"
    }],
    "clouds": {
      "all": 94
    },
    "wind": {
      "speed": 5.19,
      "deg": 218
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "d"
    },
    "dt_txt": "2020-11-17 09:00:00"
  }, {
    "dt": 1605614400,
    "main": {
      "temp": 13.1,
      "feels_like": 8.27,
      "temp_min": 13.1,
      "temp_max": 13.1,
      "pressure": 1019,
      "sea_level": 1019,
      "grnd_level": 1017,
      "humidity": 68,
      "temp_kf": 0
    },
    "weather": [{
      "id": 804,
      "main": "Clouds",
      "description": "overcast clouds",
      "icon": "04d"
    }],
    "clouds": {
      "all": 97
    },
    "wind": {
      "speed": 6.01,
      "deg": 216
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "d"
    },
    "dt_txt": "2020-11-17 12:00:00"
  }, {
    "dt": 1605625200,
    "main": {
      "temp": 12.95,
      "feels_like": 7.85,
      "temp_min": 12.95,
      "temp_max": 12.95,
      "pressure": 1018,
      "sea_level": 1018,
      "grnd_level": 1015,
      "humidity": 70,
      "temp_kf": 0
    },
    "weather": [{
      "id": 804,
      "main": "Clouds",
      "description": "overcast clouds",
      "icon": "04d"
    }],
    "clouds": {
      "all": 100
    },
    "wind": {
      "speed": 6.49,
      "deg": 209
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "d"
    },
    "dt_txt": "2020-11-17 15:00:00"
  }, {
    "dt": 1605636000,
    "main": {
      "temp": 12.36,
      "feels_like": 7.38,
      "temp_min": 12.36,
      "temp_max": 12.36,
      "pressure": 1017,
      "sea_level": 1017,
      "grnd_level": 1014,
      "humidity": 77,
      "temp_kf": 0
    },
    "weather": [{
      "id": 804,
      "main": "Clouds",
      "description": "overcast clouds",
      "icon": "04n"
    }],
    "clouds": {
      "all": 97
    },
    "wind": {
      "speed": 6.6,
      "deg": 200
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-17 18:00:00"
  }, {
    "dt": 1605646800,
    "main": {
      "temp": 12.95,
      "feels_like": 8,
      "temp_min": 12.95,
      "temp_max": 12.95,
      "pressure": 1016,
      "sea_level": 1016,
      "grnd_level": 1013,
      "humidity": 75,
      "temp_kf": 0
    },
    "weather": [{
      "id": 804,
      "main": "Clouds",
      "description": "overcast clouds",
      "icon": "04n"
    }],
    "clouds": {
      "all": 100
    },
    "wind": {
      "speed": 6.62,
      "deg": 196
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-17 21:00:00"
  }, {
    "dt": 1605657600,
    "main": {
      "temp": 11.83,
      "feels_like": 7.01,
      "temp_min": 11.83,
      "temp_max": 11.83,
      "pressure": 1014,
      "sea_level": 1014,
      "grnd_level": 1011,
      "humidity": 75,
      "temp_kf": 0
    },
    "weather": [{
      "id": 803,
      "main": "Clouds",
      "description": "broken clouds",
      "icon": "04n"
    }],
    "clouds": {
      "all": 68
    },
    "wind": {
      "speed": 6.06,
      "deg": 191
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-18 00:00:00"
  }, {
    "dt": 1605668400,
    "main": {
      "temp": 11.73,
      "feels_like": 6.47,
      "temp_min": 11.73,
      "temp_max": 11.73,
      "pressure": 1010,
      "sea_level": 1010,
      "grnd_level": 1008,
      "humidity": 71,
      "temp_kf": 0
    },
    "weather": [{
      "id": 800,
      "main": "Clear",
      "description": "clear sky",
      "icon": "01n"
    }],
    "clouds": {
      "all": 0
    },
    "wind": {
      "speed": 6.41,
      "deg": 187
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-18 03:00:00"
  }, {
    "dt": 1605679200,
    "main": {
      "temp": 12.45,
      "feels_like": 6.83,
      "temp_min": 12.45,
      "temp_max": 12.45,
      "pressure": 1007,
      "sea_level": 1007,
      "grnd_level": 1004,
      "humidity": 68,
      "temp_kf": 0
    },
    "weather": [{
      "id": 802,
      "main": "Clouds",
      "description": "scattered clouds",
      "icon": "03n"
    }],
    "clouds": {
      "all": 37
    },
    "wind": {
      "speed": 6.93,
      "deg": 178
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-18 06:00:00"
  }, {
    "dt": 1605690000,
    "main": {
      "temp": 11.83,
      "feels_like": 6.18,
      "temp_min": 11.83,
      "temp_max": 11.83,
      "pressure": 1005,
      "sea_level": 1005,
      "grnd_level": 1002,
      "humidity": 80,
      "temp_kf": 0
    },
    "weather": [{
      "id": 500,
      "main": "Rain",
      "description": "light rain",
      "icon": "10d"
    }],
    "clouds": {
      "all": 100
    },
    "wind": {
      "speed": 7.58,
      "deg": 194
    },
    "visibility": 10000,
    "pop": 1,
    "rain": {
      "3h": 1.02
    },
    "sys": {
      "pod": "d"
    },
    "dt_txt": "2020-11-18 09:00:00"
  }, {
    "dt": 1605700800,
    "main": {
      "temp": 12.07,
      "feels_like": 7.5,
      "temp_min": 12.07,
      "temp_max": 12.07,
      "pressure": 1004,
      "sea_level": 1004,
      "grnd_level": 1001,
      "humidity": 78,
      "temp_kf": 0
    },
    "weather": [{
      "id": 501,
      "main": "Rain",
      "description": "moderate rain",
      "icon": "10d"
    }],
    "clouds": {
      "all": 100
    },
    "wind": {
      "speed": 5.99,
      "deg": 267
    },
    "visibility": 10000,
    "pop": 1,
    "rain": {
      "3h": 6.54
    },
    "sys": {
      "pod": "d"
    },
    "dt_txt": "2020-11-18 12:00:00"
  }, {
    "dt": 1605711600,
    "main": {
      "temp": 9.69,
      "feels_like": 3.44,
      "temp_min": 9.69,
      "temp_max": 9.69,
      "pressure": 1007,
      "sea_level": 1007,
      "grnd_level": 1004,
      "humidity": 58,
      "temp_kf": 0
    },
    "weather": [{
      "id": 501,
      "main": "Rain",
      "description": "moderate rain",
      "icon": "10d"
    }],
    "clouds": {
      "all": 100
    },
    "wind": {
      "speed": 6.5,
      "deg": 268
    },
    "visibility": 10000,
    "pop": 1,
    "rain": {
      "3h": 3.22
    },
    "sys": {
      "pod": "d"
    },
    "dt_txt": "2020-11-18 15:00:00"
  }, {
    "dt": 1605722400,
    "main": {
      "temp": 8.66,
      "feels_like": 1.67,
      "temp_min": 8.66,
      "temp_max": 8.66,
      "pressure": 1010,
      "sea_level": 1010,
      "grnd_level": 1007,
      "humidity": 61,
      "temp_kf": 0
    },
    "weather": [{
      "id": 803,
      "main": "Clouds",
      "description": "broken clouds",
      "icon": "04n"
    }],
    "clouds": {
      "all": 80
    },
    "wind": {
      "speed": 7.49,
      "deg": 244
    },
    "visibility": 10000,
    "pop": 0.74,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-18 18:00:00"
  }, {
    "dt": 1605733200,
    "main": {
      "temp": 8.32,
      "feels_like": 1.05,
      "temp_min": 8.32,
      "temp_max": 8.32,
      "pressure": 1011,
      "sea_level": 1011,
      "grnd_level": 1008,
      "humidity": 58,
      "temp_kf": 0
    },
    "weather": [{
      "id": 800,
      "main": "Clear",
      "description": "clear sky",
      "icon": "01n"
    }],
    "clouds": {
      "all": 0
    },
    "wind": {
      "speed": 7.67,
      "deg": 241
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-18 21:00:00"
  }, {
    "dt": 1605744000,
    "main": {
      "temp": 8.23,
      "feels_like": 0.56,
      "temp_min": 8.23,
      "temp_max": 8.23,
      "pressure": 1011,
      "sea_level": 1011,
      "grnd_level": 1009,
      "humidity": 60,
      "temp_kf": 0
    },
    "weather": [{
      "id": 801,
      "main": "Clouds",
      "description": "few clouds",
      "icon": "02n"
    }],
    "clouds": {
      "all": 18
    },
    "wind": {
      "speed": 8.32,
      "deg": 239
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-19 00:00:00"
  }, {
    "dt": 1605754800,
    "main": {
      "temp": 7.66,
      "feels_like": -0.42,
      "temp_min": 7.66,
      "temp_max": 7.66,
      "pressure": 1013,
      "sea_level": 1013,
      "grnd_level": 1010,
      "humidity": 60,
      "temp_kf": 0
    },
    "weather": [{
      "id": 500,
      "main": "Rain",
      "description": "light rain",
      "icon": "10n"
    }],
    "clouds": {
      "all": 1
    },
    "wind": {
      "speed": 8.79,
      "deg": 265
    },
    "visibility": 10000,
    "pop": 0.2,
    "rain": {
      "3h": 0.16
    },
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-19 03:00:00"
  }, {
    "dt": 1605765600,
    "main": {
      "temp": 7.23,
      "feels_like": -0.15,
      "temp_min": 7.23,
      "temp_max": 7.23,
      "pressure": 1016,
      "sea_level": 1016,
      "grnd_level": 1013,
      "humidity": 59,
      "temp_kf": 0
    },
    "weather": [{
      "id": 800,
      "main": "Clear",
      "description": "clear sky",
      "icon": "01n"
    }],
    "clouds": {
      "all": 1
    },
    "wind": {
      "speed": 7.65,
      "deg": 264
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "n"
    },
    "dt_txt": "2020-11-19 06:00:00"
  }, {
    "dt": 1605776400,
    "main": {
      "temp": 7.8,
      "feels_like": 1.15,
      "temp_min": 7.8,
      "temp_max": 7.8,
      "pressure": 1016,
      "sea_level": 1016,
      "grnd_level": 1014,
      "humidity": 60,
      "temp_kf": 0
    },
    "weather": [{
      "id": 800,
      "main": "Clear",
      "description": "clear sky",
      "icon": "01d"
    }],
    "clouds": {
      "all": 9
    },
    "wind": {
      "speed": 6.78,
      "deg": 258
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "d"
    },
    "dt_txt": "2020-11-19 09:00:00"
  }, {
    "dt": 1605787200,
    "main": {
      "temp": 9.13,
      "feels_like": 3.49,
      "temp_min": 9.13,
      "temp_max": 9.13,
      "pressure": 1017,
      "sea_level": 1017,
      "grnd_level": 1014,
      "humidity": 65,
      "temp_kf": 0
    },
    "weather": [{
      "id": 802,
      "main": "Clouds",
      "description": "scattered clouds",
      "icon": "03d"
    }],
    "clouds": {
      "all": 41
    },
    "wind": {
      "speed": 5.88,
      "deg": 255
    },
    "visibility": 10000,
    "pop": 0,
    "sys": {
      "pod": "d"
    },
    "dt_txt": "2020-11-19 12:00:00"
  }, {
    "dt": 1605798000,
    "main": {
      "temp": 8.89,
      "feels_like": 4.49,
      "temp_min": 8.89,
      "temp_max": 8.89,
      "pressure": 1016,
      "sea_level": 1016,
      "grnd_level": 1013,
      "humidity": 63,
      "temp_kf": 0
    },
    "weather": [{
      "id": 500,
      "main": "Rain",
      "description": "light rain",
      "icon": "10d"
    }],
    "clouds": {
      "all": 89
    },
    "wind": {
      "speed": 3.95,
      "deg": 270
    },
    "visibility": 10000,
    "pop": 0.38,
    "rain": {
      "3h": 0.36
    },
    "sys": {
      "pod": "d"
    },
    "dt_txt": "2020-11-19 15:00:00"
  }],
  "city": {
    "id": 2643743,
    "name": "London",
    "coord": {
      "lat": 51.5085,
      "lon": -0.1257
    },
    "country": "GB",
    "population": 1000000,
    "timezone": 0,
    "sunrise": 1605338251,
    "sunset": 1605370361
  }
}]