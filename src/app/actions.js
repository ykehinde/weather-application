import store from './store'

// exports
// export const SHOW_SPINNER = "SHOW_SPINNER";
export const THROW_CALLER_ERROR = "THROW_CALLER_ERROR";
export const RECIEVE_WEATHER_DATA = "RECIEVE_WEATHER_DATA";

// constants
const API_KEY = "7fc4cd98d0a5d881cc5b2c0b0d00afea";
const API_UNIT_TYPE = "metric";
const LOCATION = "LONDON,UK"

// errors
// const MESSAGE_LOCATION_NOT_FOUND = "Location not found... ";
// const MESSAGE_API_REQUEST_ERROR = "There was a problem getting weather data. Please try again.";


export function getWeather() {
   // If weatherspinner is already open from location search, don't open it again
  //  if (!store.getState().show.weatherSpinner){
  //     store.dispatch( showSpinner('Loading Weather...') )
  //  }

   //  Build the URL to call openweather
  const url = `http://api.openweathermap.org/data/2.5/forecast?q=${LOCATION}&units=metric&appid=${API_KEY}&units=${API_UNIT_TYPE}`;

  fetch(url)
      .then( results => results.json() )
      .then( weatherData => {
        // console.log('weather', weatherData);
        store.dispatch( receiveWeatherData(weatherData) )
      })
      .catch( (error) => {
        store.dispatch(throwCallerError(error.message))
      })
}

export function receiveWeatherData(data) {
  // console.log(data);
  return {
    type: RECIEVE_WEATHER_DATA,
    data: data,
    weatherSpinnerOpen: false
  }
}

// export function showSpinner(message) {
//   return {
//     type: SHOW_SPINNER,
//     weatherSpinnerOpen: message,
//     showMoreLocations: false,
//     showWeatherHistory: false
//   }
// }

export function throwCallerError(error) {
  return {
    type: THROW_CALLER_ERROR,
    error: error
  }
}
