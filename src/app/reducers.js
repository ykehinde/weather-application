import {
  THROW_CALLER_ERROR,
  RECIEVE_WEATHER_DATA,
} from './actions'

// export const initialState = {
//   show: {
//     weatherSpinner: false,
//   },
//    data: {
//       callerError: false,
//       forecast: {
//          ready: false,
//          data: ''
//       }
//    },

// }

export const initialState = {

   // Revised state tree - makes more sense
   show: {
      weatherSpinner: false,
   },
   data: {
      callerError: false,
      forecast: {
        "cod": "",
        "message": 0,
        "cnt": 0,
        "list": [
          {
            "dt": 1596564000,
            "main": {
              "temp": 293.55,
              "feels_like": 293.13,
              "temp_min": 293.55,
              "temp_max": 294.05,
              "pressure": 1013,
              "sea_level": 1013,
              "grnd_level": 976,
              "humidity": 84,
              "temp_kf": -0.5
            },
            "weather": [
              {
                "id": 500,
                "main": "Rain",
                "description": "light rain",
                "icon": "10d"
              }
            ],
            "clouds": {
              "all": 38
            },
            "wind": {
              "speed": 4.35,
              "deg": 309
            },
            "visibility": 10000,
            "pop": 0.49,
            "rain": {
              "3h": 0.53
            },
            "sys": {
              "pod": "d"
            },
            "dt_txt": "2020-08-04 18:00:00"
          },
        ]
      }
   },

}

export function rootReducer(state = initialState, action) {
   switch(action.type){

      // case RECIEVE_WEATHER_DATA:
      //    return {
      //       ...state,
      //       data: {
      //          ...state.data,
      //          callerError: false,
      //          forecast: {
      //             ready: true,
      //             data: action.data
      //          }
      //       },
      //       show: {
      //          ...state.data.show,
      //          weatherSpinner: false,
      //       },
      //    }

      // case THROW_CALLER_ERROR:
      //    return {
      //       ...initialState,
      //       currentRoute: '/err',
      //       data: {
      //          ...state.data,
      //          callerError: action.error,
      //       },
      //    }

      default:
         return state

   }
}