import React from 'react';
import './App.css';
import Spinner from "./components/WeatherSpinner/WeatherSpinner";

import Body from "./components/Body/Body";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Holition Technical Test - Weather Forecast</h1>
      </header>
      <Spinner />
      <Body/>
    </div>
  );
}

export default App;
